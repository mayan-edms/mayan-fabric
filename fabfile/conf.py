from __future__ import unicode_literals

import os
import string
import random

from fabric.api import env
from fabric.colors import green

from .literals import (
    DB_CHOICES, DEFAULT_INSTALL_PATH, DEFAULT_DATABASE_HOST,
    DEFAULT_DATABASE_MANAGER, DEFAULT_DATABASE_NAME, DEFAULT_DATABASE_USERNAME,
    DEFAULT_OS, DEFAULT_PASSWORD_LENGTH, DEFAULT_WEBSERVER, DJANGO_DB_DRIVERS,
    OS_CHOICES, WEB_CHOICES
)
from .server_config import reduce_env


def password_generator():
    # http://snipplr.com/view/63223/python-password-generator/
    chars = string.ascii_letters + string.digits
    return ''.join(random.choice(chars) for x in range(DEFAULT_PASSWORD_LENGTH))


@reduce_env
def setup_environment():
    env['os'] = getattr(env, 'os', DEFAULT_OS)
    env['os_name'] = OS_CHOICES[env.os]
    
    env['install_path'] = getattr(env, 'install_path', DEFAULT_INSTALL_PATH[env.os])
    env['virtualenv_path'] = env['install_path']
    env['mayan_settings_path'] = os.path.join(env.install_path, 'mayan', 'settings')
    
    env['database_manager'] = getattr(env, 'database_manager', DEFAULT_DATABASE_MANAGER)
    env['database_manager_name'] = DB_CHOICES[env.database_manager]
    env['database_username'] = getattr(env, 'database_username', DEFAULT_DATABASE_USERNAME)
    env['database_password'] = getattr(env, 'database_password', password_generator())
    env['database_host'] = getattr(env, 'database_host', DEFAULT_DATABASE_HOST)
    env['drop_database'] = getattr(env, 'drop_database', False)
        
    env['database_name'] = getattr(env, 'database_name', DEFAULT_DATABASE_NAME)

    env['webserver'] = getattr(env, 'webserver', DEFAULT_WEBSERVER)
    env['webserver_name'] = WEB_CHOICES[env.webserver]

    env['django_database_driver'] = DJANGO_DB_DRIVERS[env.database_manager]



def print_supported_configs():
    print('Supported operating systems (os=): %s, default=\'%s\'' % (dict(OS_CHOICES).keys(), green(DEFAULT_OS)))
    print('Supported database managers (database_manager=): %s, default=\'%s\'' % (dict(DB_CHOICES).keys(), green(DEFAULT_DATABASE_MANAGER)))
    print('Supported webservers (webserver=): %s, default=\'%s\'' % (dict(WEB_CHOICES).keys(), green(DEFAULT_WEBSERVER)))
    print('\n')
