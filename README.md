
     __  __                          _____ ____  __  __ ____  
    |  \/  | __ _ _   _  __ _ _ __   | ____|  _ \|  \/  / ___| 
    | |\/| |/ _` | | | |/ _` | '_ \  |  _| | | | | |\/| \___ \ 
    | |  | | (_| | |_| | (_| | | | | | |___| |_| | |  | |___) |
    |_|  |_|\__,_|\__, |\__,_|_| |_| |_____|____/|_|  |_|____/ 
                  |___/             

*Mayan EDMS Fabric installation file*

    Supported operating systems (os=): [u'debian', u'ubuntu'], default='ubuntu'
    Supported database managers (database_manager=): [u'pgsql'], default='pgsql'
    Supported webservers (webserver=): [u'nginx'], default='nginx'

Available commands:

    install                             Perform a complete install of Mayan EDMS on a host
    servers                             Set destination servers or server groups by comma delimited list of names
    uninstall                           Perform a complete removal of Mayan EDMS from a host
    databases.create_database           Create the Mayan EDMS database
    databases.create_user               Create the Mayan EDMS user
    databases.drop_database             Drop Mayan EDMS's database
    databases.drop_user                 Drop Mayan EDMS's user
    mayan_edms.base_config              Create base settings/local.py file
    mayan_edms.celery_config            Tailor settings/local.py file to the task queue manager selected
    mayan_edms.collect_static           Perform Django's collectstatic command
    mayan_edms.database_config          Tailor settings/local.py file to the database manager selected
    mayan_edms.initial_setup            Setup Mayan EDMS for use
    platforms.delete_mayan              Delete Mayan EDMS from the OS
    platforms.fix_permissions           Fix installation files' permissions
    platforms.install_database_manager  Install the selected database manager
    platforms.install_dependencies      Install OS dependencies
    platforms.install_mayan             Install Mayan EDMS
    platforms.install_redis             Install the selected redis package
    platforms.install_supervisor        Installing the OS packages for the process supervisor
    platforms.install_webserver         Installing the OS packages for the webserver
    platforms.post_install              Perform post install operations
    server_config.servers               Set destination servers or server groups by comma delimited list of names
    webservers.install_site             Install Mayan EDMS site in the webserver configuration files
    webservers.reload                   Reload webserver configuration files
    webservers.remove_site              Install Mayan EDMS's site file from the webserver's configuration
    webservers.restart                  Restart the webserver

Usage
-----
Invoke the desired command using Fabric's fab executable and specify the IP address of the host on which to execute:

    fab install -H 10.0.0.1

Testing
-------
Install the vagrant fabric plugin:

    vagrant plugin vagrant-fabric
    
Provision the box with:

    vagrant up
    
Installation should progress and an install of Mayan EDMS be usable from the IP address of the provisioned box.
