from __future__ import unicode_literals

from fabric.api import env, run, settings, sudo, task
from fabric.colors import green, red


def create_database():
    """
    Create the PostgreSQL Mayan EDMS database
    """
    with settings(sudo_user='postgres'):
        sudo('createdb -O %(database_name)s %(database_username)s' % env)


def create_user():
    """
    Create the PostgreSQL Mayan EDMS user
    """
    with settings(sudo_user='postgres'):
        sudo('echo "CREATE USER %(database_username)s WITH password \'%(database_password)s\';" | psql' % env)

    print(green('Password used for Mayan EDMS database account: %s' % env.database_password, bold=True))


def drop_database():
    """
    Drop PostgreSQL's Mayan EDMS's database
    """
    print(red('Not implemented'))
    

def drop_user():
    """
    Drop PostgreSQL's Mayan EDMS's user
    """
    print(red('Not implemented'))

