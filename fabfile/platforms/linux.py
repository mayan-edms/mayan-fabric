from __future__ import unicode_literals

import os

from fabric.api import cd, env, run, settings, sudo, task


def delete_mayan():
    """
    Delete Mayan EDMS files from an Linux system
    """
    sudo('rm %(install_path)s -Rf' % env)
    

def install_mayan():
    """
    Install Mayan EDMS on an Linux system
    """

    sudo('virtualenv --no-site-packages %(install_path)s' % env)
    
    with cd(env.virtualenv_path):
        sudo('source bin/activate; pip install --upgrade distribute')
        sudo('source bin/activate; pip install mayan-edms')

    with cd(env.virtualenv_path):
        sudo('ln -sf lib/python2.7/site-packages/mayan .')


def post_install():
    """
    Post install process on a Linux systems
    """
    pass
